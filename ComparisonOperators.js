//  Comparison Operators  //


// Equal to Operator

let value = 10;
var hello = 10;

c = value == hello;
console.log(c);



// Equal value & Equal type Operator

let value1 = 10;
var hello = "10";

c = value1 === hello;
console.log(c);




// Not Equal to Operator

let value2 = 10;
var hello = "10";

c = value2 != hello;
console.log(c);



// Not Equal value & Not Equal type Operator

let val = 10;
var hello = "10";

c = val !== hello;
console.log(c);



// Greater than Operator

let value3 = 100;
var hello = 10;
c = value3 > hello;
console.log(c);



// Less than Operator

let value4 = 100;
var hello = 10;
c = value4 < hello;
console.log(c);




// Greater than or equal to Operator

let value5 = 100;
var hello = 10;
c = value5 >= hello;
console.log(c);





// Less than or equal to Operator

let value6 = 100;
var hello = 10;
c = value6 <= hello;
console.log(c);