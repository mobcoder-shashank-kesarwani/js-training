//  Arithmetic Operators  //



// Addition

var a = 10;
var b = 20;
var c = a + b;
console.log(c);




// Subtraction  

var a = 20;
var b = 10;
var c = a - b;
console.log(c);




// Multiplication

var a = 10;
var b = 20;
var c = a * b;
console.log(c);




//Exponentiation

var a = 10;
var b = 2;
var c = a ** b; 
console.log(c);





// Division

var a = 10;
var b = 2;
var c = a / b;
// console.log(c);

var d = a + b / b; //Same works in subtraction as well
var e = (a + b) / b; //Same works in subtraction as well
var f = (a * b) / b;
var g = (a ** b) / b;
console.log(d);
console.log(e);
console.log(f);
console.log(g);





// Modulus 

var a = 10;
var b = 10;
var c = a % b;
console.log(c);





// Increment

var a = 10;
var b = ++a;
console.log(b);




// Decrement

var a = 10;
var b = --a;
console.log(b);




