
//    Assignment Operator (=)  //


// = in JavaScript is used for assigning values to a variable. It is called as assignment operator. 
// The assignment operator can evaluate to the assigned value. It does not return true or false. 
// It simply assign one value of variable to another one. 



// Addition Assignment

var a = 10;
a += 20;    // a = a + 20;
console.log(a);



// Subtraction Assignment

var a = 20;
a -= 10;  // a = a - 10;
console.log(a);




// Multiplication Assignment

var a = 10;
a *= 20;  // a = a * 20;
console.log(a);





// Division Assignment

var a = 10;
a /= 2;  // a = a / 2;
console.log(a);






// Exponentiation Assignment

var a = 10;
a **= 2;  // a = a ** 2;
console.log(a);





// Modulus Assignment

var a = 10;
a %= 2;  // a = a % 2;
console.log(a);




